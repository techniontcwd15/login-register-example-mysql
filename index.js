const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const session = require('express-session')

const User = require('./models/user');

const saltRounds = 10;
const app = express();

app.use(bodyParser.json());
app.use(session({
  secret: 'keyboard cat'
}));

app.get('/is-loged', (req, res) => {
  if(req.session.user) {
    return res.json({ok: true});
  }
  
  return res.json({ok: false});
})

app.post('/login', (req, res) => {
  const username = req.body.username;
  const plainPassword = req.body.password;

  if(!username || !plainPassword) {
    res.sendStatus(500);
  }

  User.findOne({ username })
    .then(user => {
      bcrypt.compare(plainPassword, user.password, (err, isValid) => {
          if(isValid === true) {
            req.session.user = user;

            return res.sendStatus(204);
          }

          return res.sendStatus(403);
      });
    });
});

app.post('/register', (req, res) => {
  const username = req.body.username;
  const plainPassword = req.body.password;

  if(!username || !plainPassword) {
    res.sendStatus(500);
  }

  bcrypt.genSalt(saltRounds, (err, salt) => {
      bcrypt.hash(plainPassword, salt, (err, hashedPassword) => {
        User.create({ username, password: hashedPassword })
          .then(() => {
            res.sendStatus(201);
          });
      });
  });


});

app.listen(3000, async () => {
  // await User.drop();
  // await User.sync();
});
